//
//  Utils.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 02/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "Utils.h"
#include <algorithm>

//
// Generate a random int within the specifyed range
//
int Utils::getRandomInt(int lowest, int highest)
{
    // random device engine
    static std::random_device rd;
    // initialize Mersennes' twister using rd to generate the seed
    static std::mt19937 rng(rd());
    
    // generate a random numner
    std::uniform_int_distribution<int> uid(lowest, highest);
    
    return uid(rng);
}

//
// Generate a random float within the specifyed range
//
float Utils::getRandomFloat(float lowest, float highest)
{
    // random device engine
    static std::random_device rd;
    // initialize Mersennes' twister using rd to generate the seed
    static std::mt19937 rng(rd());
    
    // generate a random numner
    std::uniform_real_distribution<float> ufd(lowest, highest);
    
    return ufd(rng);
}

//
// Clamp a float to lower and upper limits
//
float Utils::clamp(float n, float lower, float upper)
{
    return std::max(lower, std::min(n, upper));
}