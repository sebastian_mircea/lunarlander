//
//  Spaceship.h
//  LunarLander
//
//  Created by Sebastian Mircea on 29/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__Spaceship__
#define __LunarLander__Spaceship__

#include "GameObject.h"
#include <algorithm>

//
// Provides player controled spaceship functionality
//
class Spaceship : public GameObject
{
public:
    // Constructor
    Spaceship(
        const ObjectType objType,      // game object type
        const GLuint textureBufferID,  // texture to use for rendering
        const float width,             // width of the ship
        const float heigth,            // height of the ship
        const Vector2& position,       // the center position
        const Vector2& velocity,       // initial velocity
        const Vector2& boundary,       // limits of the spaceship movement
        GLFWwindow *&window            // the renderer window
    );
    
    // Destructor
    ~Spaceship() {}
    
    // Update the object's state
    void update();
    
    // Handle collision with other objects
    void handleCollision(const GameObject *gameObject);
    
    // Spaceship health status getter and setter
    int health() const { return _health; }
    void setHealth(const int health) { _health = std::max(0, health); }
    
    // Safe landing status geter and setter
    bool hasLanded() const { return _hasLanded; }
    void setHasLanded(const bool hasLanded) { _hasLanded = hasLanded; }
    
    // Reset spaceship position, health and velocity
    void reset(const Vector2& position);
    
private:
    // define active engine sides
    typedef enum EngineSide {
        eNone   = 0,
        eLeft   = 1,
        eRight  = 2,
        eBoth   = 3
    } EngineSide;
    
    // Increase or decrease engine thrust
    void updateEngineThrust();
    
    // Get the engine thrust
    float engineThrust() const { return _leftEngineThrust + _rightEngineThrust; }
    
    // Set engines on or off
    void setEngineOn(const EngineSide side) { _engineOn = side; };
    
    // Apply gravity force to the spaceship
    void applyGravity();
    
    // Apply wind force to the spaceship
    void applyWind();

private:

    float _leftEngineThrust;                // thrust of the left engine
    float _rightEngineThrust;               // thrust of the right engine
    int _health;                            // spaceship health status
    EngineSide  _engineOn;                  // flag that signals which engines are on
    GLFWwindow *_window;                    // the game window
    Vector2 _boundary;                      // spaceship movements limits
    bool _isOnGround;                       // a flag that signals that the ship touches ground
    bool _hasLanded;                        // signals that the ship has landed safely
};

#endif /* defined(__LunarLander__Spaceship__) */
