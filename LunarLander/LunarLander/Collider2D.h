//
//  Collider2D.h
//  LunarLander
//
//  Created by Sebastian Mircea on 05/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__Collider2D__
#define __LunarLander__Collider2D__

#include "BoxCollider2D.h"
#include "CircleCollider2D.h"
#include "EdgeCollider2D.h"
#include <vector>

//
// Provides collision checking between game objects
//
class Collider2D
{
public:
    // Constructor
    Collider2D();
    
    // Destructor
    ~Collider2D();
    
    // Set collider position
    void setPosition(const Vector2& position);
    
    // Box collider getter and setter
    const BoxCollider2D* boxCollider() const { return _boxCollider; }
    void setBoxCollider(
            const Vector2 &position,    // position of the collider
            const float width,          // collider width
            const float height          // collider height
    );
    
    // Circle collider getter and setter
    const CircleCollider2D* circleCollider() const { return _circleCollider; }
    void setCircleCollider(
             const Vector2& position,   // position of the collider
             const float radius         // collider radius
    );
    
    // Get a pointer to the edge collider
    const EdgeCollider2D* edgeCollider() const { return _edgeCollider; }
    void setEdgeCollider(
             const std::vector<Vertex2>& vertexes    // a vector with all the height points
    );
    
    // Check for collisions
    bool isColliding(const Collider2D *collider) const;
    
private:
    // Check collision between a box collider and a circle collider
    bool areObjectsColliding(
             const BoxCollider2D* boxCollider,      // box collider object
             const CircleCollider2D* circleCollider // circle collider object
    ) const;
    
    // Check collision between a box and an edge collider
    bool areObjectsColliding(
             const BoxCollider2D* boxCollider,      // box collider object
             const EdgeCollider2D* edgeCollider     // edge collider object
    ) const;
    
    // Check collision between an edge and a circle collider
    bool areObjectsColliding(
             const CircleCollider2D* circleCollider,    // circle collider object
             const EdgeCollider2D* edgeCollider         // edge collider object
    ) const;

    //Collider2DType _colliderType;     // collider type
    BoxCollider2D* _boxCollider;        // pointer to a box collider
    CircleCollider2D* _circleCollider;  // pointer to a circle collider
    EdgeCollider2D* _edgeCollider;      // pointer to an edge collider
};

#endif /* defined(__LunarLander__Collider2D__) */
