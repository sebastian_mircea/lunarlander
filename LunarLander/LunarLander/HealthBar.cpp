//
//  HealthBar.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 15/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "HealthBar.h"

// Constructor
HealthBar::HealthBar(
               const ObjectType objType,           // game object type
               const GLuint bkTextureBufferID,     // background texture ID
               const GLuint gFillTextureBufferID,  // green fill texture ID
               const GLuint rFillTextureBufferID,  // red fill texture ID
               const GLuint oFillTextureBufferID,  // orange fill texture ID
               const float width,                  // width of the ship
               const float heigth,                 // height of the ship
               const float fillWidth,              // width of the fill                     
               const Vector2& position,            // the center position
               const GLuint fillPercent            // the percent to which the bar is filled
           ) :
GameObject(objType, gFillTextureBufferID, width, heigth, position, Vector2(0.0f, 0.0f)),
_bkTextureID(bkTextureBufferID),
_gFillTextureID(gFillTextureBufferID),
_rFillTextureID(rFillTextureBufferID),
_oFillTextureID(oFillTextureBufferID),
_fillPercent(fillPercent),
_fillWidth(fillWidth)
{
    // set fill color
    update();
}

//
// Update the object's state according to the specifyed fill percent
//
void HealthBar::update()
{
    if (_fillPercent >= 50) {
        _fgTextureID = _gFillTextureID;
    }
    else if (_fillPercent >= 25) {
        _fgTextureID = _oFillTextureID;
    }
    else {
        _fgTextureID = _rFillTextureID;
    }
}

//
// Render the health bar
//
void HealthBar::render()
{
    // render background
    setTextureBufferID(_bkTextureID);
    GameObject::render();
    // no need to go further if the fill is 0
    if (_fillPercent == 0)
        return;
    // render the fill
    setTextureBufferID(_fgTextureID);
    // set new width for the fill according to the fill percent
    float savedWidth = width();
    float newWidth = (_fillWidth * _fillPercent/100);
    setWidth(newWidth);
    // offset x position
    Vector2 savedPosition = position();
    Vector2 newPosition = savedPosition;
    // subtract half of the delta dif
    newPosition.x -= (_fillWidth - newWidth)/2;
    setPosition(newPosition);
    // render the fill
    GameObject::render();
    // restore width and position
    setWidth(savedWidth);
    setPosition(savedPosition);
}
