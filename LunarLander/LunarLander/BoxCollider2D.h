//
//  BoxCollider2D.h
//  LunarLander
//
//  Created by Sebastian Mircea on 07/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__BoxCollider2D__
#define __LunarLander__BoxCollider2D__

#include "Vector2.h"

//
// Provides AABB collider functionality
//
class BoxCollider2D
{
public:
    // Constructor
    BoxCollider2D() {}
    
    // Constructor
    BoxCollider2D(
        const Vector2 &position,    // box collider position
        const float width,          // collider width
        const float height          // collider height
    );
    
    // Collider position getter and setter
    Vector2 position() const { return _position; }
    void setPosition(const Vector2& position) { _position = position; }
    
    // Collider rectangle width getter and setter
    float width() const { return _width; }
    void setWidth(const float width) { _width = width; }
    
    // Collider rectangle height getter and setter
    float height() const {return _height; }
    void setHeight(const float height) { _height = height; }

    // Check if the box collider collides with another box collider
    bool isColliding(const BoxCollider2D *boxCollider) const;
    
private:
    Vector2 _position;              // the position of the collider
    float _width;                   // the width off the collider rectangle
    float _height;                  // the height of the collider rectangle
};

#endif /* defined(__LunarLander__BoxCollider2D__) */
