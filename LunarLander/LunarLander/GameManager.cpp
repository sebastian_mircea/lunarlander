//
//  GameManager.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 25/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "GameManager.h"
#include "Utils.h"
#include <SOIL.h>
#include <ctime>
#include <cstdlib>
#include <iostream>
#include "HealthBar.h"

// instance object initialization
std::unique_ptr<GameManager> GameManager::_instance(nullptr);

#define X_SCALE				0.7
#define Y_SCALE				0.7
#define SPACESHIP_WIDTH     50.0f	* X_SCALE
#define SPACESHIP_HEIGHT    50.0f	* Y_SCALE

#define ROCK_WIDTH          100.0f	* X_SCALE
#define ROCK_HEIGHT         100.0f	* Y_SCALE
#define ROCK_VELOCITY       -1.5f
#define ROCK_MIN_FREQUENCY  4
#define ROCK_MAX_FREQUENCY  11
#define ROCK_MIN_POS_Y      35
#define ROCK_MAX_POS_Y      95

#define UPDATES_PER_SEC     60


//TODO: screen size
//
// [private] Constructor
//
GameManager::GameManager() :
_isRunning(false),
_window(NULL),
_windowWidth(960),
_windowHeight(540),
_spaceShip(NULL),
_healthBar(NULL),
_gameStopped(true)
{
}

GameManager::~GameManager()
{
    // free game objects
    std::vector<GameObject *>::iterator gameObjIt;
    for (gameObjIt = _gameObjects.begin(); gameObjIt != _gameObjects.end(); gameObjIt++) {
        if (*gameObjIt)
            delete(*gameObjIt);
    }
    
    // delete textures
    std::map<TextureType, GLuint>::iterator texIt;
    for (texIt = _textureBufferIDs.begin(); texIt != _textureBufferIDs.end(); texIt++)
        glDeleteTextures(1, &texIt->second);
}

//
// [public] Get an instance of the GameManager
//
GameManager& GameManager::getInstance()
{
    if (_instance.get() == 0) {
        _instance.reset(new GameManager());
    }
    return *_instance;
}

//
// [public] Execute all uninit code
//
void GameManager::terminate()
{
    glfwDestroyWindow(_window);
    glfwTerminate();
}

//
// [private] Initialize GLFW
//
bool GameManager::initGLFW()
{		
    glfwInit();
    glfwWindowHint(GLFW_DEPTH_BITS, 0);
    glfwWindowHint(GLFW_RED_BITS, 4);
    glfwWindowHint(GLFW_GREEN_BITS, 4);
    glfwWindowHint(GLFW_BLUE_BITS, 4);
    glfwWindowHint(GLFW_ALPHA_BITS, 4);
    
    _window = glfwCreateWindow((int)_windowWidth, (int)_windowHeight, "Lunar Lander", NULL, NULL);
    glfwMakeContextCurrent(_window);
    
	glewExperimental = GL_FALSE;
	GLenum err = glewInit();
	if (err != GLEW_OK)
		return false;
    // set the back color to black
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    // set viewport
    glViewport(0, 0, (int)_windowWidth, (int)_windowHeight);
    
    // enable textures
    glEnable(GL_TEXTURE_2D);
    // enable alpha blending
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glMatrixMode(GL_PROJECTION);
    glOrtho(0.0f, _windowWidth, 0.0f, _windowHeight, 0.0f, 1.0f);
    glMatrixMode(GL_MODELVIEW);

	return true;
}

//
// [private] Update game objects
//
void GameManager::update()
{
    // check if we should exit the game
    if (glfwGetKey(_window, GLFW_KEY_ESCAPE))
        exit(0);
    
    // check if we should display a menu
    if (!_gameStopped && (_spaceShip->hasLanded() || _spaceShip->health() == 0)) {
        showMenu(_spaceShip->hasLanded() ? eMenuSuccessTex : eMenuGameOverTex);
        resetGame();
        _gameStopped = true;
    }
    
    if (_gameStopped) {
        // get user input and check for spacebar
        if (!glfwGetKey(_window, GLFW_KEY_SPACE))
            return;
        
        // start the game
        clearMenus();
         _spaceShip->reset(Vector2((float)(SPACESHIP_WIDTH / 2), (float)(_windowHeight - SPACESHIP_HEIGHT / 2)));
        _healthBar->setFillPercent(_spaceShip->health());
        _gameStopped = false;
    }
    
    // generate rock obstacles
    generateRocks();

    // iterate through the game objects and update them
    std::vector<GameObject *>::iterator it = _gameObjects.begin();
    // go through the rest of the objects
    GameObject *obj = NULL;
    while (it != _gameObjects.end()) {
        // update the object's state
        obj = (*it);
        obj->update();

        // check if we should delete the object
        if (obj->objectType() == eRockObject) {
            if (obj->position().x < -ROCK_WIDTH/2) {
                // it's a rock that went off screen, so we should delete it
                delete(obj);
                it = _gameObjects.erase(it);
                continue;
            }
            obj->setOrientation(obj->orientation().rotate(0.08f));
        }
        // check for collisions, avoiding collision with self
        if (obj->objectType() != eSpaceShipObject)
        {
            if (_spaceShip->collider()->isColliding(obj->collider())) {
                // handle collisions
                _spaceShip->handleCollision(obj);
                // update the health bar
                _healthBar->setFillPercent(_spaceShip->health());
            }
        }
        it++;
    }
}

//
// [public] Start the main game loop
//
void GameManager::runGame()
{
    // create all the game objects
    if (!createGameObjects())
        return;
   
    // initialize update
    double deltaTime = 0.0f;
    double lastTimeStamp = glfwGetTime();
    
    // get an iterator to the game objects
    std::vector<GameObject *>::iterator it;
    
    // start the game's main loop
    _isRunning = true;
    while (_isRunning) {
        // clear buffers to preset values
        glClear(GL_COLOR_BUFFER_BIT);
        
        // iterate through the game objects vector and render them
        it = _gameObjects.begin();
        while (it != _gameObjects.end()) {
            (*it)->render();
            it++;
        }
        
        // swap the front buffer with the one in the back
        glfwSwapBuffers(_window);
        
        // process queued events
        glfwPollEvents();
        
        double currentTimeStamp = glfwGetTime();
        deltaTime += (currentTimeStamp - lastTimeStamp) * UPDATES_PER_SEC;
        lastTimeStamp = glfwGetTime();
        while (deltaTime >= 1.0f) {
            // update game objects
            update();
            --deltaTime;
        }
        
        // check if we should exit the loop
        _isRunning = !glfwWindowShouldClose(_window);
    }
}

//
// [private] Load texture files to buffers
//
bool GameManager::loadTextures()
{
	// load textures
	std::map<TextureType, std::string> textureNames = {
		std::make_pair(TextureType::eRockTex, "res/rock.png"),
		std::make_pair(TextureType::eBackgroundTex, "res/background.png"),
		std::make_pair(TextureType::eSurfaceTex, "res/surface.png"),
		std::make_pair(TextureType::eSpaceshipTex, "res/lunarlander.png"),
		std::make_pair(TextureType::eLandingpadTex, "res/landingpad.png"),
		std::make_pair(TextureType::eHealthBarBackgroundTex, "res/healthbar_background.png"),
		std::make_pair(TextureType::eHealthBarGreenFillTex, "res/healthbar_green.png"),
		std::make_pair(TextureType::eHealthBarRedFillTex, "res/healthbar_red.png"),
		std::make_pair(TextureType::eHealthBarOrangeFillTex, "res/healthbar_orange.png"),
		std::make_pair(TextureType::eMenuNewGameTex, "res/menu_new_game.png"),
		std::make_pair(TextureType::eMenuSuccessTex, "res/menu_success.png"),
		std::make_pair(TextureType::eMenuGameOverTex, "res/menu_game_over.png")
	};

	GLuint textureBufferID = 0;
	std::map<TextureType, std::string>::iterator it;
	for (it = textureNames.begin(); it != textureNames.end(); it++)
	{
		textureBufferID = SOIL_load_OGL_texture(
			it->second.c_str(),
			SOIL_LOAD_AUTO,
			SOIL_CREATE_NEW_ID,
			SOIL_FLAG_POWER_OF_TWO
			);

		if (!textureBufferID)
			return false;

		_textureBufferIDs.insert(std::make_pair(it->first, textureBufferID));
	}

	return true;
}

//
// [private] Create all the game objects that will be rendered in the game
//
bool GameManager::createGameObjects()
{
	// load textures
	if (!loadTextures())
		return false;

    // create the background object - should be added first!!!
    GameObject *background = new GameObject(
        eBackgroundObject,
        _textureBufferIDs[TextureType::eBackgroundTex],
        _windowWidth,
        _windowHeight,
        Vector2(_windowWidth/2, _windowHeight/2),
        Vector2(0.0f, 0.0f)
    );
    
    if (!background)
        return false;
    
    // add the background object to the game objects stack
    _gameObjects.push_back(background);
    
    // create space ship - should be added seccond!!!
    _spaceShip = new Spaceship(
        eSpaceShipObject,
        _textureBufferIDs[TextureType::eSpaceshipTex],
        SPACESHIP_WIDTH,
        SPACESHIP_HEIGHT,
        Vector2((float)(SPACESHIP_WIDTH / 2), (float)(_windowHeight - SPACESHIP_HEIGHT / 2)),
        Vector2(0.0f, 0.0f),
        Vector2(_windowWidth, _windowHeight),
        _window
    );
    
    if (!_spaceShip)
        return false;
    
    _gameObjects.push_back(_spaceShip);
    
    // add a circle collider to the spaceship with a radius of 45% of the spaceship width
    _spaceShip->collider()->setCircleCollider(_spaceShip->position(), (float)(SPACESHIP_WIDTH * 0.45f));
    
    // create planet surface object
    GameObject *surface = new GameObject(
        eSurfaceObject,
        _textureBufferIDs[TextureType::eSurfaceTex],
        _windowWidth,
        (float)(221.0f * Y_SCALE),
        Vector2((float)(_windowWidth/2), (float)(221.0f/2 * Y_SCALE)),
        Vector2(0.0f, 0.0f)
    );
    
    if (!surface)
        return false;

	// load height map
	std::vector<Vertex2> heightMap;
	loadHeightMap("res/surface_cmask.png", heightMap);

    // set an edge collider
    surface->collider()->setEdgeCollider(heightMap);
    
    // add to game objects
    _gameObjects.push_back(surface);
    
    // create landing pad
    GameObject *landingPad = new GameObject(
        eLandingPadObject,
        _textureBufferIDs[TextureType::eLandingpadTex],
        100.0f,
        100.0f,
        Vector2(_windowWidth*0.435f, 20.0f),
        Vector2(0.0f, 0.0f)
    );
    
    if (!landingPad)
        return false;
    
    // add a box collider for the landing pad
    landingPad->collider()->setBoxCollider(landingPad->position(), 100.0f, 67.0f);
    // store the object
    _gameObjects.push_back(landingPad);

	// create health bar
	_healthBar = new HealthBar(
		eHealthBarObject,
		_textureBufferIDs[TextureType::eHealthBarBackgroundTex],
		_textureBufferIDs[TextureType::eHealthBarGreenFillTex],
		_textureBufferIDs[TextureType::eHealthBarRedFillTex],
		_textureBufferIDs[TextureType::eHealthBarOrangeFillTex],
		(float)(256.0f * X_SCALE),
		(float)(48.0f * Y_SCALE),
		(float)(217.0f * X_SCALE),
		Vector2(_windowWidth / 2, _windowHeight - 25),
		100
		);

	if (!_healthBar)
		return false;

	_gameObjects.push_back(_healthBar);
    showMenu(TextureType::eMenuNewGameTex);
    
    return true;
}

//
// [private] Load the height map from a mask file
//
void GameManager::loadHeightMap(const std::string fileName, std::vector<Vertex2>& heightMap)
{
	// read the mask file
	int width, height;
	unsigned char* pixels = SOIL_load_image(fileName.c_str(), &width, &height, 0, SOIL_LOAD_RGB);
	if (!pixels)
		return;
	// parse all the pixels from top to bottom and store the first black pixel
	Vertex2 point = { 0, 0 }, lastPoint = { 0, 0 }, possibleHPoint = { 0, 0 };
	int dir = 0, lastDir = 0, dirCount = 16;
	bool store = false;
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
			// get the Red component of the color
			unsigned short r = pixels[(x + y * width) * 3 + 0];
			// check if it is white or black
			if (r < 5) {
				// black pixel, so we got a height point
				point.x = (float)x; 
				point.y = (float)height - y - 5;
				// store the first and the last points
				if (x == 0 || x == width - 1)
					heightMap.push_back(point);
				// check for direction changes(-1 = down, 0 = straight, 1 = up)
				dir = (point.y - lastPoint.y > 0) - (point.y - lastPoint.y < 0);
				if (dir != lastDir) {
					lastDir = dir;
					// if we went more than 5 points in a direction, we have a possible point to store
					if (dirCount > 5)
						possibleHPoint = lastPoint;
					dirCount = 0;
					store = true;
				}
				dirCount++;
				lastPoint = point;
				// store the point only if we went more than 5 point in this direction
				if (store && dirCount > 5 && possibleHPoint.x != 0) {
					heightMap.push_back(possibleHPoint);
					store = false;
				}
				// break from the y loop, as we got our height point
				break;
			}
		}
	}

}

//
// [private] Generate rock obstacles
//
void GameManager::generateRocks()
{
    // setup rocks generator frequency
    srand((unsigned)time(0));
    static double timeOfLastGenRock = glfwGetTime();
    static int timeToNextGenRock = 1;
    
    // generate a rock, if it's that time
    if (glfwGetTime() - timeOfLastGenRock >= timeToNextGenRock)
    {
        // get a random height for the rock
        int randomHeight = Utils::getRandomInt(
            (int)_windowHeight * ROCK_MIN_POS_Y/100,
            (int)_windowHeight * ROCK_MAX_POS_Y/100
        );
        
        // create a new rock object
        GameObject *rock = new GameObject(
            eRockObject,
            _textureBufferIDs[TextureType::eRockTex],
            ROCK_WIDTH,
            ROCK_HEIGHT,
            Vector2(_windowWidth, (float)randomHeight),
            Vector2(ROCK_VELOCITY, 0.0f)
        );
        
        // add a collider to the rock
        rock->collider()->setCircleCollider(rock->position(), (float)std::max(ROCK_WIDTH*0.45f, ROCK_HEIGHT*0.45f));
        // add it to the game objects
		_gameObjects.insert(_gameObjects.end() - 1, rock);
        //_gameObjects.push_back(rock);
        // save update time
        timeOfLastGenRock = glfwGetTime();
        // set the time when the next rock will be generated
        timeToNextGenRock = Utils::getRandomInt(ROCK_MIN_FREQUENCY, ROCK_MAX_FREQUENCY);
    }
}

//
// [private] Reset the game
//
void GameManager::resetGame()
{
    // remove all the rocks and the spaceship from the game objects list
    std::vector<GameObject*>::iterator it = _gameObjects.begin();
    while (it != _gameObjects.end()) {
        GameObject *obj = *it;
        if (obj->objectType() == eRockObject) {
            // destroy the rock objects and reset the spaceship position and health
            if (obj->objectType() == eRockObject)
                delete(*it);
            it = _gameObjects.erase(it);
            continue;
        }
        it++;
    }
}

//
// [private] Show menu
//
void GameManager::showMenu(const TextureType txType)
{
    // create a new rock object
    GameObject *menu = new GameObject(
        eMenuObject,
        _textureBufferIDs[txType],
        622,
        381,
        Vector2(_windowWidth/2, _windowHeight/2),
        Vector2(0.0f, 0.0f)
    );
    // add it to the list of objects that get rendered
    _gameObjects.push_back(menu);
}

//
// [private] Clear all menus from the screen
//
void GameManager::clearMenus()
{
    // search for menu objects
    std::vector<GameObject*>::iterator it = _gameObjects.begin();
    while (it != _gameObjects.end()) {
        GameObject *obj = *it;
        if (obj->objectType() == eMenuObject)
        {
            // destroy the menu objects
            delete(*it);
            it = _gameObjects.erase(it);
            continue;
        }
        it++;
    }
}
