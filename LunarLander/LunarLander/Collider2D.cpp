//
//  Collider2D.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 05/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "Collider2D.h"


//
// [public] Constructor
//
Collider2D::Collider2D() :
_boxCollider(NULL),
_circleCollider(NULL),
_edgeCollider(NULL)
{
}

//
// [public] Destructor
//
Collider2D::~Collider2D()
{
    // cleanup colliders
    if (_boxCollider)
        delete(_boxCollider);
    if (_circleCollider)
        delete(_circleCollider);
    if (_edgeCollider)
        delete(_edgeCollider);
}

//
// [public] Set collider position
//
void Collider2D::setPosition(const Vector2& position)
{
    if (_boxCollider)
        _boxCollider->setPosition(position);
    if (_circleCollider)
        _circleCollider->setPosition(position);
}

//
// [public] Set a box collider
//
void Collider2D::setBoxCollider(const Vector2 &position,
                                const float width,
                                const float height)
{
    if (!_boxCollider)
        _boxCollider = new BoxCollider2D(position, width, height);
    else {
        _boxCollider->setPosition(position);
        _boxCollider->setWidth(width);
        _boxCollider->setHeight(height);
    }
    
}

//
// [public] Set a circle collider
//
void Collider2D::setCircleCollider(
                     const Vector2& position,   // collider position
                     const float radius)        // collider radius
{
    if (!_circleCollider)
        _circleCollider = new CircleCollider2D(position, radius);
    else {
        _circleCollider->setPosition(position);
        _circleCollider->setRadius(radius);
    }
}

//
// [public] Set an edge collider
//
void Collider2D::setEdgeCollider(const std::vector<Vertex2>& vertexes)
{
    if (!_edgeCollider)
        _edgeCollider = new EdgeCollider2D(vertexes);
    else
        _edgeCollider->setVertexes(vertexes);
}
                     
//
// [public] Checks for collisions between two colliders
//
bool Collider2D::isColliding(const Collider2D *collider) const
{
    // check if objects collide by going through all the collider types in both colliders;
    // if there are more colliders set, the last one will determine the collision
    if (_boxCollider) {
        if (collider->boxCollider())
            return _boxCollider->isColliding(collider->boxCollider());
        else if (collider->circleCollider())
            return areObjectsColliding(boxCollider(), collider->circleCollider());
        else if (collider->edgeCollider())
            return areObjectsColliding(boxCollider(), collider->edgeCollider());
    }
    else if(_circleCollider) {
        if (collider->boxCollider())
            return areObjectsColliding(collider->boxCollider(), circleCollider());
        else if (collider->circleCollider())
            return _circleCollider->isColliding(collider->circleCollider());
        else if (collider->edgeCollider())
            return areObjectsColliding(circleCollider(), collider->edgeCollider());
    }
    else if (_edgeCollider) {
        if (collider->boxCollider())
            return areObjectsColliding(collider->boxCollider(), edgeCollider());
        else if (collider->circleCollider())
            return areObjectsColliding(collider->circleCollider(), edgeCollider());
    }
    return false;
}

//
// [private] Check the collision between a box collider and a circle collider
//
bool Collider2D::areObjectsColliding(const BoxCollider2D *boxCollider,
                                     const CircleCollider2D *circleCollider) const
{
    if (!boxCollider || !circleCollider)
        return false;
    
    // collapses the four quadrants down into one
    Vector2 circleDistance;
    circleDistance.x = std::abs(circleCollider->position().x - boxCollider->position().x);
    circleDistance.y = std::abs(circleCollider->position().y - boxCollider->position().y);
    
    // check if the circle is far enough away from the rectangle
    // that no intersection is possible
    if (circleDistance.x > (boxCollider->width()/2 + circleCollider->radius()))
        return false;
    if (circleDistance.y > (boxCollider->height()/2 + circleCollider->radius()))
        return false;
    
    // check if the circle is close enough to the rectangle
    // that an intersection is guaranteed
    if (circleDistance.x < (boxCollider->width()/2))
        return true;
    if (circleDistance.y < (boxCollider->height()/2))
        return true;
    // check the case where the circle may intersect the corner of the rectangle
    // compute the distance from the center of the circle and the corner
    float cornerDistance_sq = std::pow((circleDistance.x - boxCollider->width()/2), 2) +
    std::pow((circleDistance.y - boxCollider->height()/2), 2);
    // verify that the square distance is not more than the square radius of the circle
    return (cornerDistance_sq <= std::pow(circleCollider->radius(), 2));
}

//
// [private] Check the collision between an edge collider and a circle collider
//
bool Collider2D::areObjectsColliding(const CircleCollider2D *circleCollider,
                                     const EdgeCollider2D *edgeCollider) const
{
    float Cx = circleCollider->position().x;
    float Cy = circleCollider->position().y;
    float Cr = circleCollider->radius();
    float Ax, Ay, Bx, By = 0.0f;
    std::vector<Vertex2> vertexes = edgeCollider->vertexes();
    
    // go through all segments formed by the vertexes and check for collision
    for (int i = 0; i < vertexes.size()-1; i ++)
    {
        Ax = vertexes[i].x; Ay = vertexes[i].y; Bx = vertexes[i+1].x; By = vertexes[i+1].y;
        
        float a = Bx-Ax; // line segment end point horizontal coordinate
        float b = By-Ay; // line segment end point vertical coordinate
        float c = Cx-Ax; // circle center horizontal coordinate
        float d = Cy-Ay; // circle center vertical coordinate
        
        // collision computation
        if ((d*a - c*b)*(d*a - c*b) <= Cr*Cr*(a*a + b*b)) {
            // collision is possible
            if (c*c + d*d <= Cr*Cr) {
                // line segment start point is inside the circle
                return true;
            }
            if ((a-c)*(a-c) + (b-d)*(b-d) <= Cr*Cr) {
                // line segment end point is inside the circle
                return true;
            }
            if (c*a + d*b >= 0 && c*a + d*b <= a*a + b*b) {
                // middle section only
                return true;
            }
        }
    }
    
    return false;
}

//
// [private] Check the collision between a box collider and an edge collider
//
bool Collider2D::areObjectsColliding(const BoxCollider2D *boxCollider,
                                     const EdgeCollider2D *edgeCollider) const
{
    
    return false;
}
