//
//  GameManager.h
//  LunarLander
//
//  Created by Sebastian Mircea on 25/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__GameManager__
#define __LunarLander__GameManager__

#include <memory>
#include <vector>
#include <map>
//#include <GLee.h>
#include <glew.h>
#include <glfw3.h>
#include "Spaceship.h"
#include "HealthBar.h"

//
// GameManager class provides basic game functionality(loading textures,
// creating and destroying game objects, etc.)
//
class GameManager
{
public:
    // Destructor
    ~GameManager();
    
    // Get an instance of the manager
    static GameManager& getInstance();
    
    // Uninitialize the manager
    void terminate();
    
    // Initialize GLFW
    bool initGLFW();
    
    // Run the main game loop
    void runGame();
    
private:
    // Make the constructor private to prevent creating instances of the manager outside this class
    GameManager();
    
    // define all the texture types in the game
    typedef enum TextureType {
        eBackgroundTex,
        eSpaceshipTex,
        eRockTex,
        eLandingpadTex,
        eSurfaceTex,
        eHealthBarBackgroundTex,
        eHealthBarRedFillTex,
        eHealthBarGreenFillTex,
        eHealthBarOrangeFillTex,
        eMenuNewGameTex,
        eMenuSuccessTex,
        eMenuGameOverTex
    } TextureType;
    
    // prevent copying the manager
#if __cplusplus > 199711L
    // C++ >= 11
    GameManager(GameManager const&) = delete;
    void operator=(GameManager const &) = delete;
#else
    // C++ < 11
    GameManager(GameManager const &);    // do not implement
    void operator=(GameManager const &); // do not implement
#endif
    
    // Create the game objects that will be rendered in the game
    bool createGameObjects();
    
    // Generate obstacles
    void generateRocks();
    
    // Update game objects
    void update();

    // Reset the game
    void resetGame();
    
    // Display a menu screen
    void showMenu(const TextureType txType);
    
    // Clear all menus from the screen
    void clearMenus();

	// Load textures from files to buffers
	bool loadTextures();

	// Lad the height map from a mask file
	void loadHeightMap(const std::string fileName, std::vector<Vertex2>& heightMap);

private:
    static std::unique_ptr<GameManager> _instance;      // instance of the manager
    GLFWwindow *_window;                                // a pointer to the game window
    bool _isRunning;                                    // is main game loop running
    float	_windowHeight;								// the height of the window
    float _windowWidth;									// the width of the window
    std::map<TextureType, GLuint> _textureBufferIDs;    // IDs of the game object textures
    std::vector<GameObject*>  _gameObjects;             // all the objects in the game
    Spaceship* _spaceShip;                              // a pointer to the spaceship object
    HealthBar* _healthBar;                              // a pointer tot the health bar object
    bool _gameStopped;                                  // flag that signals if the game is stopped
};

#endif /* defined(__LunarLander__GameManager__) */
