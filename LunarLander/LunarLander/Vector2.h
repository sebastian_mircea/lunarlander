//
//  Vector2.h
//  LunarLander
//
//  Created by Sebastian Mircea on 26/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__Vector2__
#define __LunarLander__Vector2__


#include <math.h>
#include <cmath>
#include <iostream>

//
// Vector2 class provides the functionality of a 2D vector
//
class Vector2
{
public:
    float x;
    float y;
    
    // Constructor
    Vector2();
    
    // Constructor that takes the vector's x and y position
    Vector2(const float x, const float y);
    
    // Sets the x and y of the vector to the specifyed values
    Vector2& set(float r, float s);
    
    // Gets the magnitude(length) of the vector
    float magnitude() const;
    
    // Normalizes the vector
    Vector2& normalize();
    
    // Measures the angle between this vector and the one specified as a parameter
    float angle(const Vector2& vector) const;
    
    // Returns the distance between two vectors
    float distance(const Vector2& vector) const;
    
    // Rotates the vector by the specifyed angle
    Vector2& rotate(float angle);
    
    // *** Overloaded operators ***
    Vector2& operator +=(const Vector2& v);
    Vector2& operator -=(const Vector2& v);
    Vector2& operator *=(float t);
    Vector2& operator /=(float t);
    Vector2& operator &=(const Vector2& v);
    Vector2 operator -() const;
    Vector2 operator +(const Vector2& v) const;
    Vector2 operator -(const Vector2& v) const;
    Vector2 operator *(float t) const;
    Vector2 operator /(float t) const;
    float operator *(const Vector2& v) const;
    Vector2 operator &(const Vector2& v) const;
    bool operator ==(const Vector2& v) const;
    bool operator !=(const Vector2& v) const;
};

//
// Constructor
//
inline Vector2::Vector2(): x(0), y(0)
{
}

//
// Constructor
//
inline Vector2::Vector2(const float x, const float y): x(x), y(y)
{
}

//
// Sets the x and y of the vector to the specifyed values
//
inline Vector2& Vector2::set(float x, float y)
{
    this->x = x;
    this->y = y;
    return (*this);
}

//
// Gets the magnitude(length) of the vector
//
inline float Vector2::magnitude() const {
    return sqrt(x*x + y*y);
}

//
// Normalizes the vector
//
inline Vector2& Vector2::normalize()
{
    return (*this /= sqrtf(x * x + y * y));
}

//
// Measures the angle between this vector and the one specified as parameter
//
inline float Vector2::angle(const Vector2 &vector) const
{
    float dot = x * vector.x + y * vector.y; // dot product
    float det = x * vector.y - y * vector.x; // determinant
    return std::atan2(det, dot);
}

//
// Returns the distance between this vector and the one specfyed as parameter
//
inline float Vector2::distance(const Vector2 &vector) const
{
    float diffX = x - vector.x;
    float diffY = y - vector.y;
    return sqrt((diffY * diffY) + (diffX * diffX));
}

//
// Rotates the vector by the specifyed angle
//
inline Vector2& Vector2::rotate(float angle)
{
    float sinus = sinf(angle);
    float cosinus = cosf(angle);
    
    float new_x = cosinus * x - sinus * y;
    float new_y = sinus * x + cosinus * y;
    
    x = new_x;
    y = new_y;
    
    return (*this);
}

//
// Operator +=
//
inline Vector2& Vector2::operator +=(const Vector2& v)
{
    x += v.x;
    y += v.y;
    return (*this);
}

//
// Operator -=
//
inline Vector2& Vector2::operator -=(const Vector2& v)
{
    x -= v.x;
    y -= v.y;
    return (*this);
}

//
// operator *=
//
inline Vector2& Vector2::operator *=(float t)
{
    x *= t;
    y *= t;
    return (*this);
}

//
// operator /=
//
inline Vector2& Vector2::operator /=(float t)
{
    float f = 1.0F / t;
    x *= f;
    y *= f;
    return (*this);
}

//
// Operator &=
//
inline Vector2& Vector2::operator &=(const Vector2& v)
{
    x *= v.x;
    y *= v.y;
    return (*this);
}

//
// Operator -
//
inline Vector2 Vector2::operator -() const
{
    return (Vector2(-x, -y));
}

//
// Operator +
//
inline Vector2 Vector2::operator +(const Vector2& v) const
{
    return (Vector2(x + v.x, y + v.y));
}

//
// Operator -
//
inline Vector2 Vector2::operator -(const Vector2& v) const
{
    return (Vector2(x - v.x, y - v.y));
}

//
// Operator *
//
inline Vector2 Vector2::operator *(float t) const
{
    return (Vector2(x * t, y * t));
}

//
// Operator /
//
inline Vector2 Vector2::operator /(float t) const
{
    float f = 1.0F / t;
    return (Vector2(x * f, y * f));
}

//
// Operator *
//
inline float Vector2::operator *(const Vector2& v) const
{
    return (x * v.x + y * v.y);
}

//
// Operator &
//
inline Vector2 Vector2::operator &(const Vector2& v) const
{
    return (Vector2(x * v.x, y * v.y));
}

//
// operator ==
//
inline bool Vector2::operator ==(const Vector2& v) const
{
    return ((x == v.x) && (y == v.y));
}

//
// operator !=
//
inline bool Vector2::operator !=(const Vector2& v) const
{
    return ((x != v.x) || (y != v.y));
}


#endif /* defined(__LunarLander__Vector2__) */
