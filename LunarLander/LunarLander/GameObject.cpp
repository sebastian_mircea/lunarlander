//
//  GameObject.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 26/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "GameObject.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

//
// [public] Constructor
//
GameObject::GameObject(
                const ObjectType objType,                           // game object type
                const GLuint textureBufferID,                       // texture buffer ID
                const float width,                                  // object width
                const float height,                                 // object height
                const Vector2& position,                            // centered position
                const Vector2& velocity,                            // object velocity
                const Vector2& orientation                          // orientation vector
            ) :
_objectType(objType),
_textureBufferID(textureBufferID),
_width(width),
_height(height),
_position(position),
_velocity(velocity),
_orientation(orientation)
{
    if (_velocity.magnitude() >= MAX_VELOCITY) {
        _velocity.normalize();
        _velocity *= MAX_VELOCITY;
    }
}

//
// [public] Render function for the game object
//
void GameObject::render()
{
    // TODO: optimize this
    
    float hWidth = _width/2;
    float hHeight = _height/2;

    // bind vertices and textures
    VertexData _vertices[] = {
        {{-hWidth,      -hHeight,       0.0f}, {0.0f, 1.0f}},
        {{hWidth,       -hHeight,       0.0f}, {1.0f, 1.0f}},
        {{hWidth,       hHeight,        0.0f}, {1.0f, 0.0f}},
        {{-hWidth,      hHeight,        0.0f}, {0.0f, 0.0f}}
    };
    // bind vertices
    GLuint vertexBufferID = 0;
    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);
    glBufferData(GL_ARRAY_BUFFER, sizeof(_vertices), _vertices, GL_STATIC_DRAW);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, sizeof(VertexData), (GLvoid *)offsetof(VertexData, positionCoordinates));
    
    // bind textures
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glTexCoordPointer(2, GL_FLOAT, sizeof(VertexData), (GLvoid *)offsetof(VertexData, textureCoordinates));
    glBindTexture(GL_TEXTURE_2D, _textureBufferID);
    
    // reset position
    glLoadIdentity();
    // translate
    glTranslatef(_position.x, _position.y, 0);
    // rotate into position, around Z axis
    glRotatef(_orientation.angle(Vector2(0.0f, 1.0f)) * (float)(-180.0 / M_PI), 0.0f, 0.0f, 1.0f);
    
    // do the drawing
    glDrawArrays(GL_QUADS, 0, 4);
    
    // free buffer space
    glDeleteBuffers(1, &vertexBufferID);
}

//
// [public] Update function for the game object
//
void GameObject::update()
{
    // uptate the object's position
    _position += _velocity;
    // update the collider position
    _collider.setPosition(_position);
}