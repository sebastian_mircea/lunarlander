//
//  Utils.h
//  LunarLander
//
//  Created by Sebastian Mircea on 02/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__Utils__
#define __LunarLander__Utils__

#include <random>

namespace Utils
{
    // Generate a random int within the specifyed range
    int getRandomInt(int lowest, int highest);
    
    // Generate a random float within the specifyed range
    float getRandomFloat(float lowest, float highest);
    
    // Clamp a float to lower and upper limits
    float clamp(float n, float lower, float upper);
};

#endif /* defined(__LunarLander__Utils__) */
