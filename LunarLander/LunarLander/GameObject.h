//
//  GameObject.h
//  LunarLander
//
//  Created by Sebastian Mircea on 26/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__GameObject__
#define __LunarLander__GameObject__

#include "Vector2.h"
#include "Collider2D.h"
//#include "GLee.h"
#include <glew.h>
#include <glfw3.h>
#include <string>

#define MAX_VELOCITY 2.0f

// Define all game object types in the game
typedef enum ObjectType
{
    eSpaceShipObject,
    eRockObject,
    eSurfaceObject,
    eLandingPadObject,
    eBackgroundObject,
    eHealthBarObject,
    eMenuObject
} ObjectType;

//
// The GameObject class provides functionality for objects visible in the game,
// including characters, terrain and others.
//
class GameObject
{
public:
    // Constructor
    GameObject(
        const ObjectType objType,                           // game object type
        const GLuint textureBufferID,                       // texture buffer ID
        const float width,                                  // object width
        const float height,                                 // object height
        const Vector2& position,                            // centered position
        const Vector2& velocity = Vector2(0.0f, 0.0f),      // object velocity
        const Vector2& orientation = Vector2(0.0f, 1.0f)    // orientation vector
    );
    
    // Destructor
    virtual ~GameObject() {}
    
    // Object type getter and setter
    ObjectType objectType() const { return _objectType; }
    void setObjectType(ObjectType objectType) { _objectType = objectType; }
    
    // Width  getter and setter
    float width() const { return _width; }
    void setWidth(const float width) { _width = width; }
    
    // Height getter and setter
    float height() const { return _height; }
    void setHeight(const float height) { _height = height; }
    
    // Vector position getter and setter
    Vector2 position() const { return _position; }
    void setPosition(const Vector2& position) { _position = position; }
    
    // Vector velocity getter and setter
    Vector2 velocity() const { return _velocity; }
    void setVelocity(const Vector2& velocity) {
        Vector2 vel = velocity;
        if (vel.magnitude() > MAX_VELOCITY)
            _velocity = vel.normalize() * MAX_VELOCITY;
        else
            _velocity = vel;
    }
    
    // Game object orientatin vector getter and setter
    Vector2 orientation() const { return _orientation; }
    void setOrientation(const Vector2& orientation) { _orientation = orientation; }
    
    // Texture buffer ID getter and setter
    GLuint textureBufferID() const { return _textureBufferID; }
    void setTextureBufferID(const GLuint textureBufferID) { _textureBufferID = textureBufferID; }
    
    // Render function for the game object
    virtual void render();
    
    // Get a pointer to the object's collider
    Collider2D* collider() { return &_collider; }
    
    // Update function for the game object
    virtual void update();
    
private:
    typedef struct {
        GLfloat positionCoordinates[3];
        GLfloat textureCoordinates[2];
    } VertexData;
    
    ObjectType _objectType;     // game object type
    //std::string _name;          // object name(used mostly for debug purposes)
    float _width;               // object width
    float _height;              // object height
    Vector2 _position;          // object position(centered)
    Vector2 _velocity;          // the velocity of the object
    Vector2 _orientation;       // orientation of the object
    GLuint _textureBufferID;    // the ID of the texture buffer associated with the object
    Collider2D _collider;       // the object's collider
};

#endif /* defined(__LunarLander__GameObject__) */
