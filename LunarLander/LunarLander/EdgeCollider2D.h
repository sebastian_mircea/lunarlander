//
//  EdgeCollider2D.h
//  LunarLander
//
//  Created by Sebastian Mircea on 07/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__EdgeCollider2D__
#define __LunarLander__EdgeCollider2D__

#include <vector>

typedef struct Vertex2
{
    float x;
    float y;
} Vertex2;

class EdgeCollider2D
{
public:
    // Constructor
    EdgeCollider2D(const std::vector<Vertex2>& vertexes);
    
    // Vertexes getter and setter
    const std::vector<Vertex2>& vertexes() const { return _vertexes; }
    void setVertexes(const std::vector<Vertex2>& vertexes) { _vertexes = vertexes; }
    
    // TODO: Check for collision with another edge collider
    bool isColliding(const EdgeCollider2D& collider) const { return false; }
    
private:
    std::vector<Vertex2> _vertexes;
};

#endif /* defined(__LunarLander__EdgeCollider2D__) */
