//
//  Spaceship.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 29/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "Spaceship.h"
#include "Utils.h"
#include <iostream>
#include <math.h>

// TODO: tweak settings
#define MAX_ENGINE_POWER            1.5f
#define ENGINE_UP_STEP              0.001f
#define ENGINE_DOWN_STEP            0.001f
#define ORIENTATION_ROTATE_FACTOR   0.008f
#define SPACESHIP_MAX_HEALTH        100
#define DAMAGE_PER_IMPACT           35



// init constants
const Vector2 _verticalVector(0.0f, 1.0f);      // a vector representing the vertical axis
const Vector2 _horizontalVector(1.0f, 0.0f);    // a vector representing the horizontal axis
const Vector2 _gravity(0.0f, -0.3f);            // a vector representing the gravitational force


//
// [public] Constructor
//
Spaceship::Spaceship(
               const ObjectType objType,      // game object type
               const GLuint textureBufferID,  // texture to use for rendering
               const float width,             // width of the ship
               const float heigth,            // height of the ship
               const Vector2& position,       // the center position
               const Vector2& velocity,       // initial velocity
               const Vector2& boundary,       // limits of the spaceship movement
               GLFWwindow *&window            // the renderer window
            ) :
GameObject(objType, textureBufferID, width, heigth, position, velocity),
_window(window),
_leftEngineThrust(0),
_rightEngineThrust(0),
_health(SPACESHIP_MAX_HEALTH),
_boundary(boundary),
_isOnGround(false),
_hasLanded(false)
{
}

//
// [public] Updates the player object
//
void Spaceship::update()
{
    if (_hasLanded)
        return;
    
    if (glfwGetKey(_window, GLFW_KEY_RIGHT)) {
        // set left engine on for turning right
        setEngineOn(eLeft);
        // reset "on ground" flag
        _isOnGround = false;
        // rotate the ship
        setOrientation(orientation().rotate(-ORIENTATION_ROTATE_FACTOR));
        // compute the force to apply
        float rotation = orientation().angle(_verticalVector);
        Vector2 force = Vector2(sin(rotation), cos(rotation)) * engineThrust();
        setVelocity(velocity() + force);
    }
    else if (glfwGetKey(_window, GLFW_KEY_LEFT)) {
        // set right engine on turing left
        setEngineOn(eRight);
        // reset "on ground" flag
        _isOnGround = false;
        // set object orientation
        setOrientation(orientation().rotate(ORIENTATION_ROTATE_FACTOR));
        // compute the force to apply
        float rotation = orientation().angle(_verticalVector);
        Vector2 force = Vector2(sin(rotation), cos(rotation)) * engineThrust();
        setVelocity(velocity() + force);
    }
    else if (glfwGetKey(_window, GLFW_KEY_UP)) {
        // set both engines on
        setEngineOn(eBoth);
        // reset "on ground" flag
        _isOnGround = false;
        // apply force
        setVelocity(velocity() + (orientation() * engineThrust()));
    }
    else
    {
        // set both engines off
        setEngineOn(eNone);
        
        // simulate gravity when engines are off
        applyGravity();
    }
    
    // apply wind
    applyWind();
    
    updateEngineThrust();
    
    // uptate the object's position
    Vector2 pos = position();
    pos += velocity();
    // clamp position to movement limits
    pos.x = Utils::clamp(pos.x, 0, _boundary.x);
    pos.y = Utils::clamp(pos.y, 0, _boundary.y);
    setPosition(pos);
    
    // update the collider position
    collider()->setPosition(pos);
}

//
// Increase or decrease engine thrust
//
void Spaceship::updateEngineThrust()
{
    // engine thrust will be increased
    // or decreased gradually to simulate inertia
    if (_engineOn == eLeft) {
        // increase left engine thrust
        _leftEngineThrust += ENGINE_UP_STEP;
    }
    else if (_engineOn == eRight) {
        // increase right engine thrust
        _rightEngineThrust += ENGINE_UP_STEP;
    }
    
    if (_engineOn == eBoth) {
        // increase the thrust of both engines
        _leftEngineThrust += ENGINE_UP_STEP;
        _rightEngineThrust += ENGINE_UP_STEP;
    }
    else { // eNone, eLeft, eRight
        if (_engineOn != eLeft) {
            // decrease left engine thrust gradually to simulate inertia
            if (_leftEngineThrust > 0)
                _leftEngineThrust -= ENGINE_DOWN_STEP * 10;
        }
        if (_engineOn != eRight) {
            // decrease right engine thrust gradually to simulate inertia
            if (_rightEngineThrust > 0)
                _rightEngineThrust -= ENGINE_DOWN_STEP * 10;
        }
    }
    Utils::clamp(_leftEngineThrust, 0, MAX_ENGINE_POWER);
    Utils::clamp(_rightEngineThrust, 0, MAX_ENGINE_POWER);
}

//
// [private] Apply gravity to the spaceship
//
void Spaceship::applyGravity()
{
    // do not apply gravity when the ship touches ground
    if (_isOnGround)
        return;
    
    // check if we shoul apply it
    static double lastUpdate = glfwGetTime();
    if (glfwGetTime() - lastUpdate < 0.2f)
        return;
    // save update time
    lastUpdate = glfwGetTime();
    // update velocity symulating gravity
    Vector2 vel = velocity();
    vel += _gravity;
    // decrease lateral force gradually
    vel.x *= 0.999999f;
    setVelocity(vel);
}

//
// [private] Apply wind force to the spaceship
//
void Spaceship::applyWind()
{
    // do not apply wind force when the ship touches ground
    if (_isOnGround)
        return;
    
    // apply wind only if the rocket is above the mountin lines
    if (position().y < 200.0f)
        return;
    
    // the wind direction will be constant
    static const Vector2 windDirection = Vector2(0.05f, 0.0f);
    
    // the wind will blow intermittently
    // the peak force will vary from flurry to flurry
    static float windPeakForce = 0;
    static float currentWindForce = 0;
    

    if (currentWindForce < windPeakForce) {
        // update the current wind force
        currentWindForce += 0.001f;
        setVelocity(velocity() + (windDirection * currentWindForce));
    }
    else {
        // start a new wind burst
        currentWindForce = 0;
        windPeakForce = Utils::getRandomFloat(0.0f, 0.4f);
    }
}

//
// [public] Handle collision with other objects
//
void Spaceship::handleCollision(const GameObject *gameObject)
{
    // give time to the spaceship to get away from the obstacle
    static float collided = false;
    static double lastUpdate = glfwGetTime();
    if (collided && glfwGetTime() - lastUpdate < 1.5f)
        return;
    
    collided = true;
    lastUpdate = glfwGetTime();
    
    if (gameObject->velocity().magnitude() > 0.0f) {
        // collision with a moving object
        Vector2 displacement = (position() - gameObject->position()).normalize();
        setVelocity((displacement * 2) + (velocity() & displacement));
        setHealth(health() - DAMAGE_PER_IMPACT);
    }
    else if (!_isOnGround){
        // collision with a stationary object(planet surface or platform)
        _isOnGround = true;
        // check if the object crahsed or landed
        if (gameObject->objectType() == eSurfaceObject) {
            // object crashed
            setHealth(health() - DAMAGE_PER_IMPACT);
        }
        else if (gameObject->objectType() == eLandingPadObject) {
            // check touchdown angle and velocity
            float landingAngle = orientation().angle(_verticalVector);
            if (std::abs(landingAngle) > 0.79) {
                // angle too big for a safe landing
                setHealth(health() - DAMAGE_PER_IMPACT);
            } else {
                // bring the ship to safe landing position
                setOrientation(_verticalVector);
                // check velocity
                if (velocity().magnitude() > 1.0f) {
                    setHealth(health() - DAMAGE_PER_IMPACT);
                }
                if (health() > 0)
                    _hasLanded = true;
            }
        }
        // stop the ship
        setVelocity(Vector2(0.0f, 0.0f));
    }
}

//
// Reset spaceship attributes
//
void Spaceship::reset(const Vector2& position)
{
    setVelocity(Vector2(0.0f, 0.0f));
    setOrientation(_verticalVector);
    setPosition(position);
    setHasLanded(false);
    setHealth(SPACESHIP_MAX_HEALTH);
    setEngineOn(eNone);
    _isOnGround = false;
}

