//
//  main.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 25/05/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

//  Defines the entry point for the application.
//


#include <iostream>
#include <Windows.h>
#include "GameManager.h"

//
// Game entry point
//
int APIENTRY WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPTSTR lpComand, int nShow) 
{
	// hide the console window
	HWND hWnd = GetConsoleWindow();
	if (IsWindow(hWnd)) ShowWindow(hWnd, SW_HIDE);

	// get an instance of the game manager
    GameManager *gameManager = &GameManager::getInstance();
    // init GLFW
	if (!gameManager->initGLFW()) 
		return 1;

    // run the main game loo[
    gameManager->runGame();
    // cleanup
    gameManager->terminate();
    
    return 0;
}
