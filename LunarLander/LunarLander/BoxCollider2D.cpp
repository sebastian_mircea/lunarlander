//
//  BoxCollider2D.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 07/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "BoxCollider2D.h"


//
// [public] Constructor
//
BoxCollider2D::BoxCollider2D(
                const Vector2 &position,    // box collider position
                const float width,          // collider width
                const float height          // collider height
                ):
_position(position),
_width(width),
_height(height)
{
}

//
// [public] Check if the box collider collides with another box collider
//
bool BoxCollider2D::isColliding(const BoxCollider2D *boxCollider) const
{
    if (!boxCollider)
        return false;
    // check for x overlap
    bool xOverlap = position().x + width()/2 <= boxCollider->position().x - boxCollider->width()/2 ||
                    position().x - width()/2 >= boxCollider->position().x + boxCollider->width()/2;
    // check for y overlap
    bool yOverlap = position().y + height()/2 <= boxCollider->position().y - boxCollider->width()/2 ||
                    position().y - height()/2 >= boxCollider->position().y + boxCollider->width()/2;
    // return collision status
    return xOverlap && yOverlap;
}
