//
//  HealthBar.h
//  LunarLander
//
//  Created by Sebastian Mircea on 15/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__HealthBar__
#define __LunarLander__HealthBar__
#include "GameObject.h"

class HealthBar : public GameObject
{
public:
    // Constructor
    HealthBar(
        const ObjectType objType,           // game object type
        const GLuint bkTextureBufferID,     // background texture ID
        const GLuint gFillTextureBufferID,  // green fill texture ID
        const GLuint rFillTextureBufferID,  // red fill texture ID
        const GLuint oFillTextureBufferID,  // orange fill texture ID
        const float width,                  // width of the ship
        const float heigth,                 // height of the ship
        const float fillWidth,              // width of the fill
        const Vector2& position,            // the center position
        const GLuint fillPercent            // the percent to which the bar is filled
    );
    
    
    // Set fill percent
    void setFillPercent(GLuint fillPercent = 100) {
        if (fillPercent <=100)
            _fillPercent = fillPercent;
    }
    
    // Update the object's state
    void update();
    
    // Render the health bar
    void render();
    
private:
    GLuint _bkTextureID;            // background texture ID
    GLuint _fgTextureID;            // foreground texture ID - it will be one of the green, red or orange IDs
    GLuint _gFillTextureID;         // green fill texture ID
    GLuint _rFillTextureID;         // red fill texture ID
    GLuint _oFillTextureID;         // orange fill texture ID
    GLuint _fillPercent;            // the percent the bar is filled to
    float _fillWidth;               // width of the fill
};

#endif /* defined(__LunarLander__HealthBar__) */
