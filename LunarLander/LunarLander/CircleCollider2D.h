//
//  CircleCollider2D.h
//  LunarLander
//
//  Created by Sebastian Mircea on 07/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#ifndef __LunarLander__CircleCollider2D__
#define __LunarLander__CircleCollider2D__

#include "Vector2.h"

class CircleCollider2D
{
public:
    // Constructor
    CircleCollider2D() {}
    
    // Constructor
    CircleCollider2D(
        const Vector2& position,    // circle collider center position
        const float radius          // circle collider radius
    );
    
    // Collider position getter and setter
    Vector2 position() const { return _position; }
    void setPosition(const Vector2& position) { _position = position; }
    
    // Collider radius getter and setter(for circle collider)
    float radius() const { return _radius; }
    void setRadius(const float radius) { _radius = radius; }
    
    // Check for collision between circle colliders
    bool isColliding(const CircleCollider2D *circleCollider) const;
    
private:
    Vector2 _position;              // the position of the collider
    float _radius;                  // the radius of the collider
};

#endif /* defined(__LunarLander__CircleCollider2D__) */
