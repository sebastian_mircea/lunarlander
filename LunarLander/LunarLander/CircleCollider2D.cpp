//
//  CircleCollider2D.cpp
//  LunarLander
//
//  Created by Sebastian Mircea on 07/06/15.
//  Copyright (c) 2015 SebastianMircea. All rights reserved.
//

#include "CircleCollider2D.h"

//
// [public] Constructor
//
CircleCollider2D::CircleCollider2D(
                    const Vector2& position,    // circle collider center position
                    const float radius          // circle collider radius
                  ) :
_position(position),
_radius(radius)
{
}

//
// [public] Check for collision between circle colliders
//
bool CircleCollider2D::isColliding(const CircleCollider2D *circleCollider) const
{
    if (!circleCollider)
        return false;
    
    //compare the distance between the colliders with the combined radius
    if (position().distance(circleCollider->position()) < _radius + circleCollider->radius())
        return true;

    return false;
}