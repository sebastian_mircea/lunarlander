# README #

This project was created as an interview test and it is my first contact with game development and OpenGL. When I started working on it I knew almost nothing about this subject.
It was initially written on a Mac in XCode and then ported to Visual Studio on Windows.
Everything was written from scratch, using OpenGL, without the aid of a game development framework. 
